// Atlas package supports ATLAS navigation messages.
package atlas

import (
	"encoding/binary"
	"time"
)

const (
	TcpTelMagic  uint32 = 0x76543210
	EdpMessageId uint32 = 0x40000
)

// Byte order for all binary messages
var ByteOrder binary.ByteOrder = binary.BigEndian

// Message length
var msgSize = binary.Size(EdpNavMessage{})

type TcpTelHeader struct {
	Magic uint32
	N     uint32
	Id    uint32
}

// Return a new message header
func Header() TcpTelHeader {
	return TcpTelHeader{
		Magic: TcpTelMagic,
		N:     uint32(msgSize),
		Id:    EdpMessageId,
	}
}

type Timestamp struct {
	Tsec  int32 `json:"tsec"`
	Tusec int32 `json:"tusec"`
}

func (t Timestamp) AsTime() time.Time {
	return time.Unix(int64(t.Tsec), int64(t.Tusec)*1000)
}

type NavValid uint16

const (
	ValidPos NavValid = (1 << iota)
	ValidDepth
	ValidAttitude
	ValidHeading
	ValidVeloOg
	ValidRates
	ValidVeloTw
	ValidDvlRange
	ValidCableOut
	ValidVesselPos
	_
	ValidVelEc
	_
	ValidAltitude
	ValidCurrent
	ValidSv
)

const ImuOk NavValid = ValidAttitude | ValidRates

type NavMode uint16

const (
	ModeAINS     NavMode = 1
	ModeLeveling         = 2
	ModeCoarse           = 3
	ModeFine             = 4
)

type EdpNavMessage struct {
	Hdr   TcpTelHeader `json:"-"`
	T     Timestamp    `json:"ts"`
	Mode  NavMode      `json:"mode"`
	Valid NavValid     `json:"valid"`
	// Latitude in radians
	Lat float64 `json:"lat"`
	// Longitude in radians
	Lon float64 `json:"lon"`
	// Depth in meters
	Depth float32 `json:"depth"`
	// Altitude in meters
	Altitude float32 `json:"alt"`
	// Roll in radians; port side up is positive
	Roll float32 `json:"roll"`
	// Pitch in radians; nose up is positive
	Pitch float32 `json:"pitch"`
	// Heading in radians True
	Heading float32 `json:"hdg"`
	// North velocity in m/s
	Vnorth float32 `json:"vn"`
	// East velocity in m/s
	Veast float32 `json:"ve"`
	// Vertical velocity in m/s
	Vdown float32 `json:"vd"`
	// Angular velocities in m/s
	Wx float32 `json:"wx"`
	Wy float32 `json:"wy"`
	Wz float32 `json:"wz"`
	// Linear accelerations in m/s^2
	Ax float32 `json:"ax"`
	Ay float32 `json:"ay"`
	Az float32 `json:"az"`
	// North component of water current
	Ncur float32 `json:"ncur"`
	// East component of  water current
	Ecur float32 `json:"ecur"`
	// Sound velocity in m/s
	Sv float32 `json:"sv"`
}

type MustNavMessage struct {
	EdpNavMessage
	// Magnetometer readings in nT
	Mx float32 `json:"mx"`
	My float32 `json:"my"`
	Mz float32 `json:"mz"`
	// DVL ranges in meters
	Ranges [4]float32 `json:"ranges"`
	// Cable out in meters
	CableOut float32 `json:"cable_out"`
	// Vessel Latitude in radians
	VLat float64 `json:"vlat"`
	// Vessel Longitude in radians
	VLon float64 `json:"vlon"`
}
