package main

import "sync"

// Safely share a value between multiple goroutines
type sharedValue struct {
	val   float32
	isNew bool
	m     sync.Mutex
}

func (s *sharedValue) Value() (float32, bool) {
	s.m.Lock()
	defer s.m.Unlock()
	ok := s.isNew
	s.isNew = false
	return s.val, ok
}

func (s *sharedValue) Set(v float32) {
	s.m.Lock()
	defer s.m.Unlock()
	s.val = v
	s.isNew = true
}

// Safely share a boolean between multiple goroutines
type sharedBool struct {
	val   bool
	isNew bool
	m     sync.Mutex
}

func (s *sharedBool) Value() (bool, bool) {
	s.m.Lock()
	defer s.m.Unlock()
	ok := s.isNew
	s.isNew = false
	return s.val, ok
}

func (s *sharedBool) Set(v bool) {
	s.m.Lock()
	defer s.m.Unlock()
	s.val = v
	s.isNew = true
}

// Safely share a slice between multiple goroutines
type sharedSlice struct {
	val   []float64
	isNew bool
	m     sync.Mutex
}

func (s *sharedSlice) Value() ([]float64, bool) {
	s.m.Lock()
	defer s.m.Unlock()
	val := make([]float64, len(s.val))
	copy(val, s.val)
	ok := s.isNew
	s.isNew = false
	return val, ok
}

func (s *sharedSlice) Set(v []float64) {
	s.m.Lock()
	defer s.m.Unlock()
	s.val = make([]float64, len(v))
	copy(s.val, v)
	s.isNew = true
}
