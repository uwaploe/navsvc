module bitbucket.org/uwaploe/navsvc

require (
	bitbucket.org/mfkenney/go-nmea v1.4.0
	bitbucket.org/uwaploe/go-focus v0.4.1
	bitbucket.org/uwaploe/go-ins/v2 v2.0.0
	bitbucket.org/uwaploe/ixblue v0.3.2
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-msgpack v0.5.3 // indirect
	github.com/hashicorp/raft v1.0.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/go-nats-streaming v0.4.2 // indirect
	github.com/nats-io/nats-server/v2 v2.2.0 // indirect
	github.com/nats-io/nats-streaming-server v0.12.2 // indirect
	github.com/nats-io/stan.go v0.8.3
	github.com/pascaldekloe/goe v0.1.0 // indirect
	github.com/prometheus/procfs v0.0.0-20190403104016-ea9eea638872 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	go.etcd.io/bbolt v1.3.2 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

go 1.13
