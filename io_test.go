package main

import (
	"bytes"
	"encoding/binary"
	"testing"
	"time"

	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
	"bitbucket.org/uwaploe/navsvc/pkg/boss"
)

func genData() atlas.MustNavMessage {
	t := time.Now().UnixNano()
	rec := atlas.MustNavMessage{}
	rec.Valid = atlas.ImuOk
	rec.T = atlas.Timestamp{
		Tsec:  int32(t / 1000000000),
		Tusec: int32((t % 1000000000) / 1000),
	}
	rec.Sv = 1500
	rec.Hdr = atlas.Header()
	return rec
}

func TestScan(t *testing.T) {
	var buf bytes.Buffer
	var padding boss.JsfEdpPadding

	rec := genData()
	hdr := boss.Header{
		Magic: boss.SonarMsgMagic,
		Type:  boss.AtlasEdpExtended,
		Size:  uint32(binary.Size(padding) + binary.Size(rec)),
	}
	err := binary.Write(&buf, boss.ByteOrder, hdr)
	if err != nil {
		t.Fatal(err)
	}
	err = binary.Write(&buf, boss.ByteOrder, padding)
	if err != nil {
		t.Fatal(err)
	}
	err = binary.Write(&buf, boss.ByteOrder, rec)
	if err != nil {
		t.Fatal(err)
	}
	expect := int(hdr.Size) + binary.Size(hdr)
	if buf.Len() != expect {
		t.Fatalf("Bad write size; expected %d, got %d", expect, buf.Len())
	}

	scanner := boss.NewScanner(&buf)
	if !scanner.Scan() {
		t.Fatalf("Scan failed: %v", scanner.Err())
	}

	if n := len(scanner.Bytes()); n != expect {
		t.Fatalf("Bad read size; expected %d, got %d", expect, n)
	}

	data := scanner.Payload()
	skip := len(padding)
	rec2 := atlas.MustNavMessage{}
	err = binary.Read(bytes.NewReader(data[skip:]), boss.ByteOrder, &rec2)
	if err != nil {
		t.Fatalf("Record read error: %v", err)
	}
	if rec2 != rec {
		t.Fatalf("Record mismatch; expected %#v, got %#v", rec, rec2)
	}
}
