// Navsvc is a UDP server which provides vehicle navigation data messages
// to the EdgeTech eBOSS sonar system.
package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"

	nmea "bitbucket.org/mfkenney/go-nmea"
	focus "bitbucket.org/uwaploe/go-focus"
	ins "bitbucket.org/uwaploe/go-ins/v2"
	"bitbucket.org/uwaploe/ixblue"
	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
	"bitbucket.org/uwaploe/navsvc/pkg/boss"
	"github.com/nats-io/stan.go"
	"github.com/vmihailenco/msgpack"
)

var Version = "dev"
var BuildDate = "unknown"

const Usage = `Usage: navsvc [options] host:port

Send UDP navigation data packets in Atlas EDP format to the eBOSS sonar
system at address HOST:PORT. Optionally start a TCP server on a local
port to supply an extended version of the EDP messages (see -port option).

`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL       string = "nats://localhost:4222"
	clusterID     string = "must-cluster"
	insSubject    string = "insv2.data"
	rovinsSubject string = "rovins.data"
	svcPort       string
	navSubject    string
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&rovinsSubject, "rovins-sub",
		lookupEnvOrString("ROVINS_SUBJECT", rovinsSubject),
		"Subject name for Rovins navigation data")
	flag.StringVar(&insSubject, "ins-sub", lookupEnvOrString("INS_SUBJECT", insSubject),
		"Subject name for INS GPS data")
	flag.StringVar(&svcPort, "port", lookupEnvOrString("NAVSVC_PORT", svcPort),
		"TCP port to provide extended navigation data")
	flag.StringVar(&navSubject, "nav-sub", lookupEnvOrString("NAV_SUBJECT", navSubject),
		"NATS subject for extended navigation data")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

// Value of ins.DataRecord.GNSSInfo[1] which indicates a valid fix
const ValidGnss uint8 = 0x1c

func setupInsSource(sc stan.Conn, subject string, newGps *sharedBool) stan.Subscription {
	var (
		sub stan.Subscription
		err error
	)

	cb := func(m *stan.Msg) {
		var rec ins.DataRecord
		err := msgpack.Unmarshal(m.Data, &rec)
		if err != nil {
			log.Printf("INS Msgpack decode error: %v", err)
		} else {
			newGps.Set((rec.GnssInfo[1] & ValidGnss) == ValidGnss)
		}
	}

	sub, err = sc.Subscribe(subject, cb)
	if err != nil {
		log.Printf("Cannot subscribe to INS data: %v", err)
	}
	return sub
}

func setupFocusSource(sc stan.Conn, vals map[string]*sharedValue) stan.Subscription {
	var (
		sub stan.Subscription
		err error
	)

	subject := os.Getenv("FOCUS_SUBJECT")
	if subject == "" {
		log.Println("FOCUS_SUBJECT not set")
		return sub
	}
	focusCb := func(m *stan.Msg) {
		var sentence string
		// Accept tagged or untagged NMEA messages produced by nmeapub.
		bs := bytes.SplitN(m.Data, []byte(":"), 2)
		if len(bs) == 2 {
			sentence = string(bs[1])
		} else {
			sentence = string(bs[0])
		}
		rec, err := focus.ParseRecord(sentence)
		if err != nil {
			log.Printf("FOCUS record parse error: %v", err)
		} else {
			for k, v := range vals {
				v.Set(rec[k])
			}
		}
	}

	sub, err = sc.Subscribe(subject, focusCb)
	if err != nil {
		log.Printf("Cannot subscribe to FOCUS data: %v", err)
	}

	return sub
}

// Parse decimal degrees from the string representation of
// degrees and decimal minutes
func dmToDegrees(deg, dm, hemi string) float64 {
	ideg, err := strconv.ParseUint(deg, 10, 16)
	if err != nil {
		return 0.
	}
	min, err := strconv.ParseFloat(dm, 64)
	if err != nil {
		return 0.
	}

	var sign float64
	if hemi == "W" || hemi == "S" {
		sign = -1
	} else {
		sign = 1
	}

	return (float64(ideg) + min/60.) * sign
}

func setupGpsSource(sc stan.Conn, pos *sharedSlice) stan.Subscription {
	var (
		sub stan.Subscription
		err error
	)

	subject := os.Getenv("VESSEL_GPS_SUBJECT")
	if subject == "" {
		log.Println("VESSEL_GPS_SUBJECT not set")
		return sub
	}
	tag := os.Getenv("VESSEL_GPS_TAG")

	gpsCb := func(m *stan.Msg) {
		var sentence []byte
		// Accept tagged or untagged NMEA messages produced by nmeapub.
		bs := bytes.SplitN(m.Data, []byte(":"), 2)
		if len(bs) == 2 {
			// Tag must match
			if string(bs[0]) != tag {
				return
			}
			sentence = bs[1]
		} else {
			sentence = bs[0]
		}
		s, err := nmea.ParseSentence(sentence)
		if err != nil {
			log.Printf("NMEA sentence parse error: %v", err)
		} else {
			var p [2]float64
			switch s.Id {
			case "GPRMC":
				p[1] = dmToDegrees(s.Fields[2][0:2], s.Fields[2][2:], s.Fields[3])
				p[0] = dmToDegrees(s.Fields[4][0:3], s.Fields[4][3:], s.Fields[5])
				pos.Set(p[:])
			case "GPGGA":
				p[1] = dmToDegrees(s.Fields[1][0:2], s.Fields[1][2:], s.Fields[2])
				p[0] = dmToDegrees(s.Fields[3][0:3], s.Fields[3][3:], s.Fields[4])
				pos.Set(p[:])
			}
		}
	}

	sub, err = sc.Subscribe(subject, gpsCb)
	if err != nil {
		log.Printf("Cannot subscribe to Vessel GPS data: %v", err)
	}

	return sub
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	sc, err := stan.Connect(clusterID, "nav-svc", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	conn, err := net.Dial("udp", args[0])
	if err != nil {
		log.Fatalf("Cannot access eBOSS at %s", args[0])
	}

	// Subscribe to FOCUS data messages (vehicle depth and altitude)
	values := map[string]*sharedValue{
		"cableLength": &sharedValue{},
	}
	if sub := setupFocusSource(sc, values); sub != nil {
		defer sub.Unsubscribe()
	}

	// Subscribe to the vessel GPS messages.
	pos := &sharedSlice{}
	if sub := setupGpsSource(sc, pos); sub != nil {
		defer sub.Unsubscribe()
	}

	newGps := &sharedBool{}
	if sub := setupInsSource(sc, insSubject, newGps); sub != nil {
		defer sub.Unsubscribe()
	}

	// Broker to manage TCP clients
	broker := NewBroker()
	// Construct the message header and allocate the buffer
	var padding boss.JsfEdpPadding
	size := binary.Size(padding) + binary.Size(atlas.MustNavMessage{})
	hdr := boss.Header{
		Magic: boss.SonarMsgMagic,
		Type:  boss.AtlasEdpExtended,
		Size:  uint32(size),
	}
	buf := bytes.NewBuffer(make([]byte, 0, size+binary.Size(hdr)))

	// Callback to process the INS messages. Each incoming message triggers
	// the creation of a new Atlas EDP message which is sent to the eBOSS
	// sonar system via UDP
	insCb := func(m *stan.Msg) {
		rec := ixblue.DataRecord{}
		rec.UnmarshalBinary(m.Data)
		msg := newNavMsg(rec, newGps)
		// Include the most recent FOCUS and vessel-GPS data
		updateNavMsg(&msg, values["cableLength"], pos)

		// Pack into a BOSS (JSF) message
		binary.Write(buf, boss.ByteOrder, hdr)
		buf.Write(padding[:])
		binary.Write(buf, boss.ByteOrder, msg)
		if navSubject != "" {
			sc.Publish(navSubject, buf.Bytes())
		}

		// Forward to the Broker which will pass it along to any TCP clients
		select {
		case broker.Notifier <- buf.Bytes():
		default:
		}
		buf.Reset()
		// Send the standard message to eBOSS
		binary.Write(conn, atlas.ByteOrder, msg.EdpNavMessage)
	}

	sub, err := sc.Subscribe(rovinsSubject, insCb)
	if err != nil {
		log.Fatalf("Cannot subscribe to Rovins data: %v", err)
	}
	defer sub.Unsubscribe()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("NAV message service starting (%s)", Version)

	if svcPort != "" {
		ln, err := net.Listen("tcp", ":"+svcPort)
		if err != nil {
			log.Fatalf("Cannot start TCP service: %v", err)
		}
		defer ln.Close()
		log.Printf("Listening on %s", ln.Addr())

		go func() {
			for {
				client, err := ln.Accept()
				if err != nil {
					return
				}
				log.Printf("Client connection from %s", client.RemoteAddr())
				go broker.HandleConnection(client)
			}
		}()
	}

	// Exit on a signal
	<-sigs
}
