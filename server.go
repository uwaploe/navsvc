package main

import (
	"log"
	"net"
)

type Broker struct {
	// Channel to receive messages which will be forwarded
	// to all registered clients.
	Notifier chan []byte
	// New client connections
	newClient chan chan []byte
	// Closed connections
	closeClient chan chan []byte
	// Client registry
	registry map[chan []byte]bool
}

func (b *Broker) listen() {
	for {
		select {
		case c := <-b.newClient:
			b.registry[c] = true
			log.Printf("Client registered [%d]", len(b.registry))
		case c := <-b.closeClient:
			close(c)
			delete(b.registry, c)
			log.Printf("Client removed [%d]", len(b.registry))
		case ev := <-b.Notifier:
			for ch, _ := range b.registry {
				select {
				case ch <- ev:
				default:
				}
			}
		}
	}
}

// NewBroker creates a new Broker and starts it listening for atlas.MustNavMessages
func NewBroker() *Broker {
	b := &Broker{
		Notifier:    make(chan []byte, 1),
		newClient:   make(chan chan []byte),
		closeClient: make(chan chan []byte),
		registry:    make(map[chan []byte]bool),
	}

	go b.listen()

	return b
}

func (b *Broker) HandleConnection(conn net.Conn) {
	ch := make(chan []byte, 1)
	b.newClient <- ch
	defer func() {
		b.closeClient <- ch
	}()

	for msg := range ch {
		_, err := conn.Write(msg)
		if err != nil {
			log.Printf("Error writing message: %v", err)
			return
		}
	}
}
