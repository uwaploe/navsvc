package main

import (
	"math"
	"time"

	"bitbucket.org/uwaploe/ixblue"
	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
)

func validGps(status ixblue.AlgStatus) bool {
	if (status.Word[1]&ixblue.AlgManualGpsValid) != 0 ||
		(status.Word[0]&ixblue.AlgGpsValid) != 0 {
		return true
	}

	return false
}

// Create a new (extended) Atlas navigation message from a Rovins standard binary
// data record. NewGps is true if a new fix was obtained from the vehicle GNSS.
func newNavMsg(dr ixblue.DataRecord, newGps *sharedBool) atlas.MustNavMessage {
	m := atlas.MustNavMessage{}
	m.Hdr = atlas.Header()

	t := dr.Timestamp()
	if t.IsZero() {
		t = time.Now()
	}

	nsecs := t.UnixNano()
	m.T.Tsec = int32(nsecs / 1e9)
	m.T.Tusec = int32((nsecs % 1e9) / 1000)

	status := ixblue.AlgStatus{}
	if err := dr.ExtractBlocks(&status); err != nil {
		// Set all bits to 1
		status.Word[0] = 0xffffffff
	}

	pos := ixblue.Position{}
	if err := dr.ExtractBlocks(&pos); err == nil {
		m.Lat = pos.Lat * math.Pi / 180
		// Rovis longitude is East 0-360
		if pos.Lon > 180 {
			pos.Lon -= 360.
		}
		m.Lon = pos.Lon * math.Pi / 180
		m.Depth = -pos.Alt
		if validGps(status) {
			m.Valid |= atlas.ValidPos
			m.Valid |= atlas.ValidDepth
		}
	}

	att := ixblue.AttitudeHdg{}
	if err := dr.ExtractBlocks(&att); err == nil {
		m.Roll = att.Roll * math.Pi / 180
		// iXBlue: bow-down positive
		// Atlas: bow-up positive
		m.Pitch = -att.Pitch * math.Pi / 180
		m.Heading = att.Heading * math.Pi / 180
		m.Valid |= (atlas.ValidHeading | atlas.ValidAttitude)
	}

	speed := ixblue.GeoSpeed{}
	if err := dr.ExtractBlocks(&speed); err == nil {
		m.Vnorth = speed.North
		m.Veast = speed.East
		m.Vdown = -speed.Up
		m.Valid |= (atlas.ValidVeloOg | atlas.ValidVelEc)
	}

	rotation := ixblue.VehRotRate{}
	if err := dr.ExtractBlocks(&rotation); err == nil {
		// Atlas convention:
		//    Wx = change in heading; clockwise positive
		//    Wy = change in pitch; bow-up positive
		//    Wz = change in roll; port-up positive
		m.Wx = -rotation.Xv3
		m.Wy = -rotation.Xv2
		m.Wz = rotation.Xv1
		m.Valid |= atlas.ValidRates
	}

	acc := ixblue.VehAccel{}
	if err := dr.ExtractBlocks(&acc); err == nil {
		m.Ax = acc.Xv1
		m.Ay = -acc.Xv2
		m.Az = -acc.Xv3
	}

	dvl := ixblue.DvlGndSpeed{}
	if err := dr.ExtractBlocks(&dvl); err == nil {
		m.Altitude = dvl.Alt
		if (status.Word[0] & ixblue.AlgDvlValid) == ixblue.AlgDvlValid {
			m.Valid |= atlas.ValidAltitude
		}
	}

	if (m.Valid & atlas.ValidDepth) == 0 {
		depth := ixblue.DepthData{}
		if err := dr.ExtractBlocks(&depth); err == nil {
			m.Depth = depth.Depth
			m.Valid |= atlas.ValidDepth
		}
	}

	cur := ixblue.Current{}
	if err := dr.ExtractBlocks(&cur); err == nil {
		m.Ncur = cur.North
		m.Ecur = cur.East
		m.Valid |= atlas.ValidCurrent
	}

	sv := ixblue.SoundVel{}
	if err := dr.ExtractBlocks(&sv); err == nil {
		m.Sv = sv.Svel
		m.Valid |= atlas.ValidSv
	}

	return m
}

// Update the (extended) Atlas message with Cable Out, and Vessel GPS data.
func updateNavMsg(m *atlas.MustNavMessage, cl *sharedValue,
	pos *sharedSlice) {
	var ok bool

	m.CableOut, ok = cl.Value()
	if ok {
		m.Valid |= atlas.ValidCableOut
	}
	if p, ok := pos.Value(); ok {
		m.VLat = p[1] * math.Pi / 180.0
		m.VLon = p[0] * math.Pi / 180.0
		m.Valid |= atlas.ValidVesselPos
	}
}
