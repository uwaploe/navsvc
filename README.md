# MuST Navigation Server

This program provides real-time navigation data to an EdgeTech eBOSS sonar in the form of ATLAS EDP data packets. It also provides a TCP server which supplies an extended version of the navigation data for the MuST Beamformer.

The input navigation data is read from a [NATS Streaming Server](https://nats-io.github.io/docs/nats_streaming/intro.html) and is provided by the INS, DVL, FOCUS towbody, and vessel GPS.

## Output Data Formats

The output is a stream of EdgeTech JSF messages. The body of each message is an extended ATLAS EDP Data Record, the message number (type code) is 4037. Below is the definition of the JSF message header:

``` c++
/* The standard message header consists of the following fields           */ 
/* which is abstracted from the SonarMessageHeaderType data structure:    */ 
/*                                                                        */ 
/* Bytes 0:1: Start of message.  This is the value 0x1601 and can be used */ 
/*            to validate the message structure.                          */ 
/* Byte 2:    Version number.  Version number of protocol in use.         */ 
/* Byte 3:    Session ID.  Not used in BOSS.                              */ 
/* Bytes 4:5: Sonar Message Number.  This is the unique number for the    */ 
/*            type of data to follow.  See description below for details  */ 
/* Byte 6:    Sonar Command Field.  Not used in BOSS.                     */ 
/* Byte 7:    Sub System: 0 for BOSS.                                     */ 
/* Byte 8:    Channel: 0 unless otherwise noted for BOSS.                 */ 
/* Byte 9-11: Reserved for BOSS and unused.                               */ 
/* Byte 12-15:Bytes to follow                                             */ 
```

### ATLAS EDP Data Record

**Note:** To maintain compatibility with the eBOSS JSF output, there are 12 bytes of "padding" between the end of the JSF message header and the start of the ATLAS data record.

``` c++
/* Magic word (find start of telegram)                                      */
#define TCPTEL_MAGIC    0x76543210

/* Message id for atlas epd message in telId field */
#define ATLAS_EDP_MESSAGE_ID (0x40000)

/*--------------------------------------------------------------------------*/
/* Values for navValid field                                                */
/*--------------------------------------------------------------------------*/

typedef enum
{
  EDD_NAV_VALID_POS       =  0x0001,  /* Position valid bit               */
  EDP_NAV_VALID_DEPTH     =  0x0002,  /* Depth valid bit                  */
  EDP_NAV_VALID_ATT       =  0x0004,  /* Attitude (roll, pitch) valid bit */
  EDP_NAV_VALID_HEAD      =  0x0008,  /* Heading valid bit                */
  EDP_NAV_VALID_VELO_OG   =  0x0010,  /* Velocity over ground valid bit   */
  EDP_NAV_VALID_RATES_ACC =  0x0020,  /* Angular rates valid bit          */
  EDP_NAV_VALID_VELO_TW   =  0x0040,  /* Velocity through water valid bit */

  EDP_NAV_VEL_EARTH_COORDS=  0x0800,  /* Velocity over ground in ship coor*/
                                      /* otherwise in earth coords.       */
                                      /* Applies to fvNorthOG, fvEastOG,  */
                                      /* fvDownOG.  fvNorthOG becomes fwd */
                                      /* fvEastOG becoms stbd, fvDownOG   */
                                      /* becomes downward if bit is set.  */

  EDP_NAV_VALID_ALTITUDE  =  0x2000,  /* Altitude valid bit               */
  EDP_NAV_VALID_SEACURRENT=  0x4000,  /* Current valid bit                */
  EDP_NAV_VALID_SOUNDVELO =  0x8000,  /* Sound velocity valid bit         */

} AtlasEdpMsgValidityValues;

typedef struct TcpTelHead_T
{
  UINT32      magic;    /* Magic word = TCPTEL_MAGIC                       */
  UINT32      telLen;   /* Telegram length (inclusive this header) [byte]  */
  UINT32      telId;    /* Telegram identifier. ATLAS_EDP_MESSAGE_ID       */
} TcpTelHead_T;

typedef struct
{
  int    tv_sec;         /* seconds */
  int    tv_usec;        /* and microseconds */
} TimeStamp_T;

typedef struct
{
  /* Telegram header                                                        */
  TcpTelHead_T head;

  /* Reference time for message data                                        */
  TimeStamp_T TimeStamp;

  /* Navigation mode: 1: AINS, 2: Leveling, 3: Coarse, 4: Fine.             */
  UINT16  navMode;

  /* Valid bits. (Uses NAV_VALID_xxx)                                       */
  UINT16  navValid;

  /* Latitude and longitude in radians                                      */
  double  dLatitudeRadians;
  double  dLongitudeRadians;

  /* Depth in meters                                                        */
  float   fDepth;

  /* Altitude in meters                                                     */
  float   fAltitude;

  /* Roll in radians: Port side higher then stbd => positive value          */
  float   fRollRadians;

  /* Pitch in radians,  Nose (bow) up => postive value                      */
  float   fPitchRadians;

  /* Heading in radians, north=0, clockwise                                 */
  float   fHeadingRadians;

  /* Velocities in meters m/S references to Compass N                       */
  float   fvNorthOG;
  float   fvEastOG;
  float   fvDownOG;

  /* Angular accelerometers in radiangs per second                          */
  float   fwx;  /*  rad/s   delta heading         */
  float   fwy;  /* rad/s    delta down/up         */
  float   fwz;  /* rad/s    delta right/left      */

  /* Linear accelerometers in meters per second squared                     */
  float   fax; /* m/S2     acc ahead            */
  float   fay; /* m/S2     acc right            */
  float   faz; /* m/S2     acc down             */

  /* Estimate of sea currents.  Typically about 0                           */
  float   fvNCur;  /* m/S */
  float   fvECur;  /* m/S */

  /* Sound velocity in meters / second                                      */
  float   fWaterSoundVelocity;

}   EDPMSG_NAV;
```

### Extended Navigation Record

The extended record adds the following fields to the end of EDPMSG_NAV.

``` c++
// Magnetometer values in nano-Teslas
float   fmx;
float   fmy;
float   fmz;
// DVL ranges (1 per beam) in meters
float   franges[4];
// Winch cable-out in meters
float   fcableOut;
// Vessel latitude in radians
double  dvLat;
// Vessel longitude in radians
double  dvLon;
```

The following additional bit assignments are added to
AtlasEdpMsgValidityValues.

``` c++
MUST_NAV_VALID_DVLRANGE = 0x0080
MUST_NAV_VALID_CABLEOUT = 0x0100
MUST_NAV_VALID_VPOS = 0x0200
```
